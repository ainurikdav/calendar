/**
@license
Copyright (c) 2018 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
Code distributed by Google as part of the polymer project is also
subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
*/

import { html } from 'lit-element';
import { PageViewElement } from './page-view-element.js';

// These are the shared styles needed by this element.
import { SharedStyles } from './shared-styles.js';

class MyView1 extends PageViewElement {

  
  static get properties() {
    return {
      _clicks: { type: Number },
      _month: { type: Number },
      _days: { type: Number },
      _value: { type: Number },
      _monthNames: {type: Array },
    };
}
  
  static get styles() {
    return [
      SharedStyles
    ];

}
constructor() {
  super();
  this.D1 = new Date(),
  this.D1last = new Date(this.D1.getFullYear(),this.D1.getMonth()+1,0).getDate(), // последний день месяца
  this.D1Nlast = new Date(this.D1.getFullYear(),this.D1.getMonth(),this.D1last).getDay(), // день недели последнего дня месяца
  this.D1Nfirst = new Date(this.D1.getFullYear(),this.D1.getMonth(),1).getDay(); // день недели первого дня месяца
  this.calendarAll = [];
  this.calendarCount = 0;
  this.calendarAll[this.calendarCount] = document.createElement('tr');
  this._month = ["Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь"]; 
  
  if (this.D1Nfirst != 0) {
     for(let  i = 1; i < this.D1Nfirst; i++) this.calendarAll[this.calendarCount].appendChild(document.createElement('td'));
  } else{ // если первый день месяца выпадает на воскресенье, то требуется 7 пустых клеток 
    for(let  i = 0; i < 6; i++) this.calendarAll[this.calendarCount].appendChild(document.createElement('td'));
  }

  // дни месяца
  for(let  i = 1; i <= this.D1last; i++) {
  if (i != this.D1.getDate()) {
    let monthDay = document.createElement('td');
    monthDay.innerHTML = i;
    this.calendarAll[this.calendarCount].appendChild(monthDay) 
  } else {
    let monthDay = document.createElement('td');
    monthDay.className =  'today';
    monthDay.innerHTML = i;
    this.calendarAll[this.calendarCount].appendChild(monthDay)
  
  }

  if (new Date(this.D1.getFullYear(),this.D1.getMonth(),i).getDay() == 0) {  // если день выпадает на воскресенье, то перевод строки
    this.calendarCount += 1;
    this.calendarAll[this.calendarCount] = document.createElement('tr');
    }
  }

  // пустые клетки после последнего дня месяца
  if (this.D1Nlast != 0) {
    for(var  i = this.D1Nlast; i < 7; i++) {
    this.calendarAll[this.calendarCount].appendChild(document.createElement('td'));
    }
  }
}

  render() {
    return html`
    <table id="calendar2">
    <thead>
      <tr>
        <td colspan="7">${this._month[this.D1.getMonth()]} ${this.D1.getFullYear()}</td>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Пн</td><td>Вт</td><td>Ср</td><td>Чт</td><td>Пт</td><td>Сб</td><td>Вс</td>
      </tr>
      ${this.calendarAll.map(i =>  i)}
    </tbody>
  </table>`;
  }
   
  
  _getDateInfo() {
  
  
  }
}


window.customElements.define('my-view1', MyView1);