/**
@license
Copyright (c) 2018 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
Code distributed by Google as part of the polymer project is also
subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
*/

import { css } from 'lit-element';

export const SharedStyles = css`
  :host {
    display: block;
    box-sizing: border-box;
  }

  section {
    padding: 24px;
    background: var(--app-section-odd-color);
  }

  section > * {
    max-width: 300px;
    margin-right: auto;
    margin-left: auto;
  }

  section:nth-of-type(even) {
    background: var(--app-section-even-color);
  }

  h2 {
    font-size: 24px;
    text-align: center;
    color: var(--app-dark-text-color);
  }

  @media (min-width: 460px) {
    h2 {
      font-size: 36px;
    }
  }

  .circle {
    display: block;
    width: 64px;
    height: 64px;
    margin: 0 auto;
    text-align: center;
    border-radius: 50%;
    background: var(--app-primary-color);
    color: var(--app-light-text-color);
    font-size: 30px;
    line-height: 64px;
  }

  #calendar2 {
    width: 600px;
    font: monospace;
    line-height: 1.2em;
    font-size: 15px;
    text-align: center;
    margin: 0 auto;
  }
  #calendar2 thead tr td {
    font-size: 24px;
    color: #000;
    text-align: left;
    padding: 20px 60px;
  }
  #calendar2 thead tr:nth-child(1) td:nth-child(2) {
    color: rgb(50, 50, 50);
  }
  
  #calendar2 tbody td {
    color: rgb(44, 86, 122);
    padding: 15px 0;
  }

  #calendar2 tbody td:nth-child(n+6), #calendar2 .holiday {
    color: rgb(231, 140, 92);
  }
  
  #calendar2 tbody td.today {
    color: #fff !important;
    background: #ff000d;
    border-radius: 20px 20px 20px 20px;
  }

  @media (max-width: 700px) {
    #calendar2 {
      width: 280px;
    }
  }
`;
